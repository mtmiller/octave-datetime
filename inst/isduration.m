## Copyright (C) 2018 Mike Miller
##
## This file is part of Octave.
##
## Octave is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {} isduration (@var{x})
## @seealso{datetime, duration, isdatetime}
## @end deftypefn

function ret = isduration (x)

  if (nargin != 1)
    print_usage ();
  endif

  ret = (isobject (x) && isa (x, "duration"));

endfunction

%!assert (isduration ([]), false)
%!assert (isduration (1), false)
%!assert (isduration (1:10), false)
%!assert (isduration (datetime ()), false)
%!assert (isduration (hours (1)), true)

## Test input validation
%!error isduration ()
%!error isduration (1, 2)
