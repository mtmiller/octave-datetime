## Copyright (C) 2018 Mike Miller
##
## This file is part of Octave.
##
## Octave is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {} seconds (@var{x})
## @seealso{days, duration, hours, milliseconds, minutes, years}
## @end deftypefn

function y = seconds (x)

  if (nargin != 1)
    print_usage ();
  endif

  if (! isnumeric (x))
    error ("seconds: X must be a numeric array or a duration array");
  endif

  if (isnumeric (x))
    y = duration (0, 0, x, "Format", "s");
  endif

endfunction

%!assert (isobject (seconds (0)))
%!assert (isa (seconds (0), "duration"))
%!assert (size (seconds (0)), [1, 1])
%!assert (seconds (seconds (1:10)), 1:10)

## Test input validation
%!error seconds ()
%!error seconds (1, 2)
%!error seconds ("s")
