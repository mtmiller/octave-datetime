## Copyright (C) 2016-2018 Mike Miller
##
## This file is part of Octave.
##
## Octave is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*-

function disp (self)

  if (strcmp (self.Format, "default"))
    fmt = "yyyy-mm-dd HH:MM:SS";
  elseif (strcmp (self.Format, "defaultdate"))
    fmt = "yyyy-mm-dd";
  else
    fmt = self.Format;
  endif

  v = [self.Year, self.Month, self.Day, self.Hour, self.Minute, self.Second];
  s = datestr (v, fmt);
  disp (s);

endfunction

